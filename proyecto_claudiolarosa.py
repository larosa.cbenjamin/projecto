import random
import time
#Claudio La Rosa Navarrete 20.834.565-6 13/10/2021

#la primera funcion definida es la que nos permite crear la matriz
def crearmatriz ():
    matriz = []
    contadorparticula = 1
    contadorlista = 1
    contadorA = 1
    contadorB = 1
    contadorE = 1
    contadorF = 1
    contadorS = 1
    
    for i in range(10):
        
        if contadorlista == 1 or contadorlista == 10:
            lista = []
            for i in range(8):
                lista.append(0)
            contadorlista = contadorlista + 1
        else:
            lista = []
            for i in range(8):
                letra = random.randint(0,10)
                if letra == 1:
                    if contadorparticula == 1:
                        lista.append(1)
                        contadorparticula = contadorparticula + 1
                    elif contadorparticula > 1:
                        lista.append(0)   

                elif letra == 2:
                    if contadorA <= 10:
                        lista.append(2)
                        contadorA = contadorA + 1
                    elif contadorA > 10:
                        lista.append(0)   
  
                elif letra == 3:
                    if contadorB <= 6:
                        lista.append(5)
                        contadorB = contadorB + 1
                    elif contadorB > 6:
                        lista.append(0)   

                elif letra == 4:
                    if contadorE <= 5:
                        lista.append(3)
                        contadorE = contadorE + 1
                    elif contadorE > 5:
                        lista.append(0)   

                elif letra == 5:
                    if contadorF <= 2:
                        lista.append(4)
                        contadorF = contadorF + 1
                    elif contadorF > 2:
                        lista.append(0)   
 
                elif letra == 6:
                    if contadorS <= 6:
                        lista.append(6)
                        contadorS = contadorS + 1
                    elif contadorS > 6:
                        lista.append(0)   
 
                elif letra == 0 or letra > 6:
                    lista.append(0)

            contadorlista = contadorlista + 1                   
        matriz.append(lista)
    return matriz

#Debido a que es una matriz numerica, la funcion imprimirmatriz genera una interfaz grafica interpretando los numeros y asignandole valores
def imprimirmatriz (matriz):
    contador1 = 1
    contador2 = 1
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            if contador1 <= 8:
                if contador2 <= 7:
                    print("___", end=" ")
                    contador2 = contador2 + 1

                elif contador2 > 7:
                    print("___")
                    contador2 = 1
                contador1 = contador1 + 1
            elif contador1 > 8 and contador1 <= 72:
                if contador2 <= 7:
                    if matriz[i][j] == 0:
                        print("| |", end=" ")
                        contador2 = contador2 + 1
                    if matriz[i][j] == 1:
                        print("|*|", end=" ")
                        contador2 = contador2 + 1    
                    if matriz[i][j] == 2:
                        print("|A|", end=" ")
                        contador2 = contador2 + 1
                    if matriz[i][j] == 3:
                        print("|E|", end=" ")
                        contador2 = contador2 + 1
                    if matriz[i][j] == 4:
                        print("|F|", end=" ")
                        contador2 = contador2 + 1
                    if matriz[i][j] == 5:
                        print("|B|", end=" ")
                        contador2 = contador2 + 1
                    if matriz[i][j] == 6:
                        print("|S|", end=" ")
                        contador2 = contador2 + 1                
                elif contador2 > 7:
                    if matriz[i][j] == 0:
                        print("| |")
                        contador2 = 1
                    if matriz[i][j] == 1:
                        print("|*|")
                        contador2 = 1    
                    if matriz[i][j] == 2:
                        print("|A|")
                        contador2 = 1
                    if matriz[i][j] == 3:
                        print("|E|")
                        contador2 = 1
                    if matriz[i][j] == 4:
                        print("|F|")
                        contador2 = 1
                    if matriz[i][j] == 5:
                        print("|B|")
                        contador2 = 1
                    if matriz[i][j] == 6:
                        print("|S|")
                        contador2 = 1       
                        
                contador1 = contador1 + 1
            elif contador1 > 72 and contador1 <= 80:
                if contador2 <= 7:
                    print("¯¯¯", end=" ")
                    contador2 = contador2 + 1

                elif contador2 > 7:
                    print("¯¯¯")
                    contador2 = 1
                contador1 = contador1 + 1

#Con esta funcion se puede obtener la posicion de la particula, devuelve las coordenadas de la misma                
def buscarparticula(matriz):
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            if matriz[i][j] == 1:
                return i,j

#Para poder moverse primeramente se comprueba si existe alguna letra
def comprobarefecto(m, c1, c2, s):
    if s == 1:
        c1 = c1 - 1
        if c1 == 0:
            return 0
        else:
            movimiento = m[c1][c2]
            if movimiento > 1:
                return 1
            else: 
                return 0
    if s == 2:
        c2 = c2 + 1
        if c2 == 8:
            return 0
        else:
            movimiento = m[c1][c2]
            if movimiento > 1:
                return 1
            else: 
                return 0 
    if s == 3:
        c1 = c1 + 1
        if c1 == 9:
            return 0
        else:
            movimiento = m[c1][c2]
            if movimiento > 1:
                return 1
            else: 
                return 0 
    if s == 4:
        c2 = c2 - 1
        if c2 == -1:
            return 0
        else:
            movimiento = m[c1][c2]
            if movimiento > 1:
                return 1
            else: 
                return 0
    if s == 5:
        c1 = c1 - 1
        c2 = c2 - 1
        if c1 == 0: 
            return 0 
        if c2 == -1:
            return 0
        else:
            movimiento = m[c1][c2]
            if movimiento > 1:
                return 1
            else: 
                return 0
    if s == 6:
        c1 = c1 - 1
        c2 = c2 + 1
        if c1 == 0:
            return 0 
        if c2 == 8:
            return 0
        else:
            movimiento = m[c1][c2]
            if movimiento > 1:
                return 1
            else: 
                return 0                                               
    if s == 7:
        c1 = c1 + 1 
        c2 = c2 + 1
        if c1 == 9:
            return 0
        if c2 == 8:
            return 0
        else:
            movimiento = m[c1][c2]
            if movimiento > 1:
                return 1
            else: 
                return 0 
    if s == 8:
        c1 = c1 + 1
        c2 = c2 - 1
        if c1 == 9:
            return 0 
        if c2 == -1:
            return 0
        else:    
            movimiento = m[c1][c2]
            if movimiento> 1:
                return 1
            else: 
                return 0         

#en caso de que exista se procede a identificar la letra
def comprobartipo(m,c1,c2,s):
    if s == 1:
        respuesta = m[c1 - 1][c2]
        return respuesta
    if s == 2:
        respuesta = m[c1][c2 + 1]
        return respuesta  
    if s == 3:
        respuesta = m[c1 + 1][c2]
        return respuesta
    if s == 4:
        respuesta = m[c1][c2 - 1]
        return respuesta
    if s == 5:
        respuesta = m[c1 - 1][c2 - 1]
        return respuesta
    if s == 6:
        respuesta = m[c1 - 1][c2 + 1]
        return respuesta                                               
    if s == 7:
        respuesta = m[c1 + 1][c2 + 1]
        return respuesta
    if s == 8:
        respuesta = m[c1 + 1][c2 - 1]
        return respuesta

def main ():
    matriz = []
    backupmatriz = []
    termino = 0
    efectoS = 0
    contadorS = 0
    matriz = crearmatriz ()
    backupmatriz = matriz
    imprimirmatriz (matriz)
    origen = buscarparticula(matriz)
    coord1 = origen[0]
    coord2 = origen[1]
    
    sentido = random.randint(1, 8)
    
    for i in range(10):
        time.sleep(3)
        efecto = comprobarefecto(matriz, coord1, coord2, sentido)
        if efecto == 1:
            efecto1 = comprobartipo (matriz, coord1, coord2, sentido)
        else:
            efecto1 = 0
    
        #Este if permite en caso de que la particula interactue con una S aplicar el efecto de la letra
        #(un error que no pude arreglar es que al volver a la matriz original se genera otra particula)
        if efectoS == 0:
            efectoS = 0
        elif efectoS == 1:
            contadorS = contadorS + 1
            if contadorS == 6:
                matriz = backupmatriz
                efectoS = 0
                co1 = origen[0]
                co2 = origen[1]
                matriz[co1][co2] = 0
                matriz[coord1][coord2] = 1

        #Mediante este if se dan las instrucciones para las 8 direcciones posibles ademas de en caso de chocar con alguna de las paredes que la particula realice un rebote
        if efecto == 0 or efecto1 == 5:            
            if sentido == 1:
                coord1 = coord1 - 1
                if coord1 == 0:
                    coord1 = coord1 + 1
                    sentido = random.randint(1,8) 
                else:
                    matriz[coord1 + 1][coord2] = 0    
                    matriz[coord1][coord2] = 1
                    if efecto1 == 5:
                        coord1 = coord1 - 1
                        if coord1 > 0:
                            matriz [coord1][coord2] = 5
                            coord1 = coord1 + 1
            if sentido == 2:
                coord2 = coord2 + 1
                if coord2 == 8:
                    coord2 = coord2 - 1
                    sentido = random.randint(1,8) 
                else:
                    matriz[coord1][coord2 - 1] = 0    
                    matriz[coord1][coord2] = 1
                    if efecto1 == 5:
                        coord2 = coord2 + 1
                        if coord2 < 8:
                            matriz [coord1][coord2] = 5
                            coord2 = coord2 - 1
            if sentido == 3:
            
                coord1 = coord1 + 1
                if coord1 == 9:
                    coord1 = coord1 - 1
                    sentido = random.randint(1,8) 
                else:    
                    matriz[coord1 - 1][coord2] = 0
                    matriz[coord1][coord2] = 1     
                    if efecto1 == 5:
                        coord1 = coord1 + 1
                        if coord1 < 9:
                            matriz [coord1][coord2] = 5
                            coord1 = coord1 - 1
            if sentido == 4:
                coord2 = coord2 - 1
                if coord2 == -1:
                    coord2 = coord2 + 1
                    sentido = random.randint(1,8) 
                else: 
                    matriz[coord1][coord2 + 1] = 0   
                    matriz[coord1][coord2] = 1        
                    if efecto1 == 5:
                        coord2 = coord2 - 1
                        if coord2 > 0:
                            matriz [coord1][coord2] = 5
                            coord2 = coord2 + 1
            if sentido == 5:
                coord1 = coord1 - 1
                coord2 = coord2 - 1
                if coord1 == 0 and coord2 == -1:
                    coord1 = coord1 + 1
                    coord2 = coord2 + 1
                    sentido = random.randint(1,8)
                elif coord1 == 0 and coord2 != -1:
                    coord1 = coord1 + 1
                    coord2 = coord2 + 1
                    sentido = random.randint(1,8)
                elif coord1 != 0 and coord2 == -1:
                    coord1 = coord1 + 1
                    coord2 = coord2 + 1
                    sentido = random.randint(1,8)
                else:        
                    matriz[coord1 + 1][coord2 + 1] = 0
                    matriz[coord1][coord2] = 1
                    if efecto1 == 5:
                        coord1 = coord1 - 1
                        coord2 = coord2 - 1
                        if coord1 > 0 and coord2 > -1:
                            matriz [coord1][coord2] = 5
                            coord1 = coord1 + 1
                            coord2 = coord2 + 1
            if sentido == 6:
            
                coord1 = coord1 - 1
                coord2 = coord2 + 1
                if coord1 == 0 and coord2 == 8:
                    coord1 = coord1 + 1
                    coord2 = coord2 - 1
                    sentido = random.randint(1,8)
                elif coord1 == 0 and coord2 != 8:
                    coord1 = coord1 + 1
                    coord2 = coord2 - 1
                    sentido = random.randint(1,8)
                elif coord1 != 0 and coord2 == 8:
                    coord1 = coord1 + 1
                    coord2 = coord2 - 1
                    sentido = random.randint(1,8)
                else:
                    matriz[coord1 + 1][coord2 - 1] = 0
                    matriz[coord1][coord2] = 1    
                    if efecto1 == 5:
                        coord1 = coord1 - 1
                        coord2 = coord2 + 1
                        if coord1 > 0 and coord2 < 8:
                            matriz [coord1][coord2] = 5
                            coord1 = coord1 + 1
                            coord2 = coord2 - 1

            if sentido == 7:
                coord1 = coord1 + 1
                coord2 = coord2 + 1
                if coord1 == 9 and coord2 == 8:
                    coord1 = coord1 - 1
                    coord2 = coord2 - 1
                    sentido = random.randint(1,8)
                elif coord1 == 9 and coord2 != 8:
                    coord1 = coord1 - 1
                    coord2 = coord2 - 1
                    sentido = random.randint(1,8)
                elif coord1 != 9 and coord2 == 8:
                    coord1 = coord1 - 1
                    coord2 = coord2 - 1
                    sentido = random.randint(1,8)
                else:
                    matriz[coord1 - 1][coord2 - 1] = 0
                    matriz[coord1][coord2] = 1
                    if efecto1 == 5:
                        coord1 = coord1 + 1
                        coord2 = coord2 + 1
                        if coord1 < 9 and coord2 < 8:
                            matriz [coord1][coord2] = 5
                            coord1 = coord1 - 1
                            coord2 = coord2 - 1
            if sentido == 8:
                coord1 = coord1 + 1
                coord2 = coord2 - 1
                if coord1 == 9 and coord2 == -1:
                    coord1 = coord1 - 1
                    coord2 = coord2 + 1
                    sentido = random.randint(1,8)
                elif coord1 == 9 and coord2 != -1:
                    coord1 = coord1 - 1
                    coord2 = coord2 + 1
                    sentido = random.randint(1,8)
                elif coord1 != 9 and coord2 == -1:
                    coord1 = coord1 - 1
                    coord2 = coord2 + 1
                    sentido = random.randint(1,8)
                else:
                    matriz[coord1 - 1][coord2 + 1] = 0
                    matriz[coord1][coord2] = 1
                    if efecto1 == 5:
                        coord1 = coord1 + 1
                        coord2 = coord2 - 1
                        if coord1 < 9 and coord2 > -1:
                            matriz [coord1][coord2] = 5
                            coord1 = coord1 - 1
                            coord2 = coord2 + 1      
            imprimirmatriz(matriz)

        #Con este if si existe una letra que interactua con la otra se aplicara su efecto
        elif efecto == 1:
            if efecto1 == 2:
                sentido = random.randint(1,8)
            elif efecto1 == 3:
                coordenadactual = buscarparticula(matriz)
                c1 = coordenadactual[0]
                c2 = coordenadactual[1]
                matriz[c1][c2] = 0
                coord1 = origen[0]
                coord2 = origen[1]
                matriz[coord1][coord2] = 1
                sentido = random.randint (1,8)
            
            elif efecto1 == 4:
                break

            elif efecto1 == 6:
                coordenadactual = buscarparticula(matriz)
                coord1 = coordenadactual[0]
                coord2 = coordenadactual[1]            
                efectoS = 1
                matriz = []
                for i in range(10):
                    lista = []
                    for i in range(8):
                        lista.append(0)
                    matriz.append(lista)
                matriz[coord1][coord2] = 1    
            imprimirmatriz(matriz)


if __name__ == "__main__":
    main ()

